import 'package:flutter/material.dart';
import 'package:projectbucketlist/About.dart';
import 'package:projectbucketlist/ui/home.dart';

class Open extends StatefulWidget {
  @override
  _OpenState createState() => _OpenState();
}

class _OpenState extends State<Open> {
  String nama = "";
  String job = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Container(
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                  color: Colors.amber,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(40),
                      bottomRight: Radius.circular(40))),
              padding: EdgeInsets.only(top: 85),
              height: 230,
              width: 410,
              child: Column(
                children: [
                  Text(
                    "BucketList",
                    style: TextStyle(
                        fontSize: 40,
                        color: Colors.grey[900],
                        fontWeight: FontWeight.bold,
                        fontFamily: "Delicate"),
                  ),
                  SizedBox(height: 17),
                  Text(
                    "'Create Your BucketList And Get All Your Dreams'",
                    style: TextStyle(
                        fontSize: 17,
                        fontStyle: FontStyle.italic,
                        fontWeight: FontWeight.w300,
                        color: Colors.grey[900]),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 80,
            ),
            Container(
              padding: EdgeInsets.only(left: 10, right: 10),
              child: Column(
                children: [
                  Text(
                    "What's your name?",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextField(
                    onChanged: (txt) {
                      setState(() {
                        nama = txt;
                      });
                    },
                    keyboardType: TextInputType.name,
                    decoration: InputDecoration(
                      hintText: 'Name',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Text(
                    "What's your job?",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextField(
                    onChanged: (txt) {
                      setState(() {
                        job = txt;
                      });
                    },
                    keyboardType: TextInputType.name,
                    decoration: InputDecoration(
                      hintText: 'Job',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 148, right: 148),
              alignment: Alignment.center,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ListBucket(nama: nama, job: job)),
                  );
                },
                padding: EdgeInsets.only(top: 10, bottom: 10),
                color: Colors.amber,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Next',
                      style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w600,
                          color: Colors.grey[900]),
                    ),
                    SizedBox(width: 2),
                    Icon(
                      Icons.keyboard_arrow_right,
                      size: 29,
                      color: Colors.grey[900],
                    )
                  ],
                ),
              ),
            ),
            SizedBox(height: 10),
            TextButton.icon(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return About();
                  }));
                },
                style: TextButton.styleFrom(primary: Colors.grey[200]),
                icon: Icon(Icons.help),
                label: Text("Developer")),
          ],
        ),
      ),
    ));
  }
}
